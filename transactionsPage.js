import React from 'react';
import ReactDOM from 'react-dom';
import './css/transactionsPage.css';
import Prof from "./profilePage";

class TransactionsPage extends React.Component {
    render() {
        return (
            <div className="transactionsPage">
               <p><button type="submit" onClick={()=>ReactDOM.render(<Prof/>,
                    document.getElementById('root'))}>Back</button></p>
            </div>
        );
    }
}

// export default TransactionsPage;