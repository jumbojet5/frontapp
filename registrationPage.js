import React from 'react';
import ReactDOM from 'react-dom';
import './css/registrationPage.css';
import Prof from "./profilePage";


class RegistrationPage extends React.Component {
    constructor(props){
        super(props);
        this.state={token:{}}
    }
    /*eslint-env jquery*/
    handleSubmit(newData) {
        $.ajax({
            url: '/user/signup',
            dataType: 'json',
            type: 'POST',
            data: newData,
            success: function(data) {
                this.setState({token: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('/user/signup', status, err.toString());
            }
        });
    }

    editData(){
        const login=document.getElementById("loginInput").value;
        const password=document.getElementById("passwordInput").value;
        const firstName=document.getElementById("firstNameInput").value;
        const lastName=document.getElementById("lastNameInput").value;
        const interests=document.getElementById("interestsInput").value;
        if (!login || !password || !firstName || !lastName || !interests) {
            return false;
        }
        this.handleSubmit({
            login:login,
            password:password,
            firstName:firstName,
            lastName:lastName,
            interests:interests});
        return true;
    }

    render() {
        return (
            <div className="registrationPage">
                <p className="registrationPage-title">Sign up</p>
                <p>Login</p>
                <input className="registrationPage-input" type="text" size={40} id="loginInput"/>
                <p>Password</p>
                <input className="registrationPage-input" type="password" size={40} id="passwordInput"/>
                <p>First name</p>
                <input className="registrationPage-input" type="text" size={40} id="firstNameInput"/>
                <p>Last name</p>
                <input className="registrationPage-input" type="text" size={40} id="lastNameInput"/>
                <p>Interests</p>
                <input className="registrationPage-input" type="text" size={40} id="interestsInput"/>
                <p><button className="registrationPage-button" type="submit" onClick={()=>{
                    if(this.editData())
                        ReactDOM.render(<Prof token={this.state.token}/>, document.getElementById('root'))
                }}>Confirm</button></p>
            </div>
        );
    }
}


export default RegistrationPage;