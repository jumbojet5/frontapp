import React from 'react';
import ReactDOM from 'react-dom';
import './css/editProfilePage.css';
import Prof from "./profilePage";

class Temp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data : {
                text: ""
            }
        };
    }

    changeState(){
        this.setState({text:"new text"});
    }
    /*eslint-env jquery*/
    handleSubmit() {
        var text={text:"text"};
        $.ajax({
            url: '/temp',
            dataType: 'json',
            data: text,
            type: 'POST',
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('/temp', status, err.toString());
                this.setState({text: 'text'});
            }
        });
    }

    componentDidMount() {
    }
    render(){
        return(
            <div>
                <p className="one">{this.state.data.text}</p>
                <button type="submit" onClick={()=>ReactDOM.render(<Prof/>,
                    document.getElementById('root'))}>text</button>
                <button type="button" onClick={()=>{
                    var text=document.getElementById("field").value;
                    this.setState({data:{text:text}});
                    // return(<p className="other">{this.state.data.text}</p>);
                }}>te </button>
                <p className="one">{this.state.data.text}</p>
                <input type="text" id="field"/>
            </div>
        );
    }
}


export default Temp;