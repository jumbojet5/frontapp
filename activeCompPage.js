import React from 'react';
import ReactDOM from 'react-dom';
import './css/activeCompPage.css';
import Prof from "./profilePage";
import CompetitionForm from "./competitionForm";

class ActiveCompPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
        this.token="";
    }
    /*eslint-env jquery*/
    loadFromServer() {
        $.ajax({
            url: "/challenges/my",
            dataType: 'json',
            cache: false,
            headers:this.token,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("/challenges/my", status, err.toString());
            }
        });
    }

    componentDidMount() {
        this.loadFromServer();
        setInterval(this.loadCommentsFromServer, 2000);
    }

    render() {
        this.token=this.props.token;
        const compNodes = this.state.data.map(function(comp) {
            return (
                <form>
                    <CompetitionForm challenge={comp}/>
                </form>
            )
        });
        return (
            <div className="activeCompPage">
                {compNodes}
                <p><button className="activeCompPage-button" type="submit" onClick={()=>ReactDOM.render(
                    <Prof token={this.token}/>, document.getElementById('root'))}>Back</button></p>
            </div>
        );
    }
}


export default ActiveCompPage;