import React from 'react';
import ReactDOM from 'react-dom';
import './css/editProfilePage.css';
import Prof from "./profilePage";

class EditProfilePage extends React.Component {
    constructor(props){
        super(props);
        this.token="";
    }
    /*eslint-env jquery*/
    handleSubmit(newData) {
    $.ajax({
        url: '/user/profile',
        dataType: 'json',
        type: 'POST',
        data: newData,
        headers:this.token,
        // success: function(data) {
        //     this.setState({token: data});
        // }.bind(this),
        error: function(xhr, status, err) {
            console.error('/user/profile', status, err.toString());
        }
    });
    }

    editData(){
        this.token=this.props.token;
        const login=document.getElementById("loginInput").value;
        const password=document.getElementById("passwordInput").value;
        const firstName=document.getElementById("firstNameInput").value;
        const lastName=document.getElementById("lastNameInput").value;
        const interests=document.getElementById("interestsInput").value;
        if (!login || !password || !firstName || !lastName || !interests) {
            return false;
        }
        this.handleSubmit({
            login:login,
            password:password,
            firstName:firstName,
            lastName:lastName,
            interests:interests});
        return true;
    }

    render() {
        return (
            <div className="editProfilePage">
                <p className="editProfilePage-title">Edit</p>
                <p>Login</p>
                <input type="text" size={40} id="loginInput"/>
                <p>Password</p>
                <input type="password" size={40} id="passwordInput"/>
                <p>First name</p>
                <input type="text" size={40} id="firstNameInput"/>
                <p>Last name</p>
                <input type="text" size={40} id="lastNameInput"/>
                <p>Interests</p>
                <input type="text" size={40} id="interestsInput"/>
                <p><button className="editProfilePage-button" type="submit" onClick={()=>{
                    if(this.editData())
                        ReactDOM.render(<Prof token={this.token}/>, document.getElementById('root'));}
                }>Confirm</button>
                    <button className="editProfilePage-button" type="button" onClick={()=>ReactDOM.render(
                        <Prof token={this.token}/>, document.getElementById('root'))}>Back</button></p>
            </div>
        );
    }
}


export default EditProfilePage;