import React from 'react';
import ReactDOM from 'react-dom';
import './css/invitesPage.css';
import Prof from "./profilePage";
import CompetitionForm from "./competitionForm";

class InvitesPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [],
            displayB: "none"
        };
        this.id=0;
        this.token="";
    }
    /*eslint-env jquery*/
    loadFromServer() {
        $.ajax({
            url: "/challenges",
            dataType: 'json',
            cache: false,
            headers:this.token,
            success: function(data) {
                this.setState({data : data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("/challenges", status, err.toString());
            }
        });
    }

    handleSubmit(newData) {
        $.ajax({
            url: "/challenges/"+this.id,
            dataType: 'json',
            type: 'POST',
            data: newData,
            headers:this.token,
            // success: function(data) {
            //     this.setState({token: data});
            // }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }
        });
        $.ajax({
            url: "/challenges/"+this.id+"/participate",
            dataType: 'json',
            cache: false,
            headers:this.token,
            error: function(xhr, status, err) {
                console.error("/challenges/"+this.id+"/participate", status, err.toString());
            }
        });
    }

    editData(){
        const link=document.getElementById("inputLink").value;
        if (!link) {
            return false;
        }
        this.handleSubmit({link:link});
        return true;
    }

    componentDidMount() {
        this.loadFromServer();
        setInterval(this.loadFromServer, 2000);
    }

    render() {
        this.token=this.props.token;
        const compNodes = this.state.data.map(function(comp) {
            return (
                <div>
                    <CompetitionForm challenge={comp} title="Waiting for Acceptance"/>
                    <p><button type="submit" onClick={()=>{
                        this.id = comp.id;
                        this.setState({displayB:"block"});
                    }}>Accept</button></p>
                </div>
            )
        });
        return (
            <div className="invitesPage">
                {compNodes}
                <div style={{display:this.state.displayB}}>
                    <p className="createCompPage-title">Your Link</p>
                    <input className="invitesPage-input" type="text" size={40} id="inputLink"/>
                    <p><button className="invitesPage-button" type="submit" onClick={()=>{
                        if(this.editData())
                            this.setState({displayB:"none"})
                    }}>Confirm</button></p>
                </div>
                <p><button className="invitesPage-button" type="submit" onClick={()=>ReactDOM.render(<Prof token={this.token}/>,
                    document.getElementById('root'))}>Back</button></p>
            </div>
        );
    }
}


export default InvitesPage;