import React from 'react';
import ReactDOM from 'react-dom';
import './css/createCompPage.css';
import Prof from "./profilePage";

class UserForm extends React.Component{
    render(){
        return(
            <div>
                <p>Login: {this.props.user.login}, Interests: {this.props.user.interests}</p>
                <p>First Name: {this.props.user.login}, Last Name: {this.props.user.interests}</p>
            </div>
        );
    }
}

class CreateCompPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [],
            displayB: "none",
            userAcceptor:{}
        };
        this.token="";
    }
    /*eslint-env jquery*/
    loadFromServer() {
        $.ajax({
            url: "/user/list",
            dataType: 'json',
            cache: false,
            headers:this.token,
            success: function(data) {
                this.setState({data : data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("/user/list", status, err.toString());
            }
        });
    }
    handleSubmit(newData) {
        $.ajax({
            url: '/challenges',
            dataType: 'json',
            type: 'POST',
            data: newData,
            headers:this.token,
            // success: function(data) {
            //     this.setState({token: data});
            // }.bind(this),
            error: function(xhr, status, err) {
                console.error('/challenges', status, err.toString());
            }
        });
    }

    editData(){
        const title=document.getElementById("inputTitle").value;
        const description=document.getElementById("inputDescription").value;
        const bet=document.getElementById("inputSumBet").value;
        const link=document.getElementById("inputLink").value;
        if (!title || !description || !bet || !link) {
            return false;
        }
        this.handleSubmit({
            title:title,
            description:description,
            betSum:bet,
            userCreator: this.props.userCreator,
            userAcceptor: this.state.userAcceptor,
            creatorLink: link
        });
        return true;
    }

    componentDidMount() {
        this.loadFromServer();
        setInterval(this.loadFromServer, 2000);
    }


    render() {
        this.token=this.props.token;
        const compNodes = this.state.data.map(function(comp) {
            return (
                <div>
                    <UserForm user={comp}/>
                    <p><button type="submit" onClick={()=>this.setState({displayB:"block", userAcceptor:comp})}>
                        Create Competition</button></p>
                </div>
            )
        });

        return (
            <div className="createCompPage">
                <p>{compNodes}</p>
                <div className="createCompPage-newDiv" id="temp" style={{display:this.state.displayB}}>
                    <p className="createCompPage-title">Title</p>
                    <input className="createCompPage-input" type="text" size={40} id="inputTitle"/>
                    <p className="createCompPage-title">Description</p>
                    <input className="createCompPage-input" type="text" size={40} id="inputDescription"/>
                    <p className="createCompPage-title">SumBet</p>
                    <input className="createCompPage-input" type="text" size={40} id="inputSumBet"/>
                    <p className="createCompPage-title">Link</p>
                    <input className="createCompPage-input" type="text" size={40} id="inputLink"/>
                    <p><button className="activeCompPage-button" type="submit" onClick={()=>{
                        if(this.editData())
                            this.setState({displayB:"none"})
                    }}>Confirm</button>
                        <button onClick={()=>{
                            document.getElementById("inputTitle").value="";
                            this.setState({displayB:"none"})
                        }}>Cancel</button></p>
                </div>
                <p><button className="activeCompPage-button" type="submit" onClick={()=>ReactDOM.render(
                    <Prof token={this.token}/>, document.getElementById('root'))}>Back</button></p>
            </div>
        );
    }
}


export default CreateCompPage;