import React from 'react';
import './css/compForm.css';

class CompetitionForm extends React.Component{
    render() {
        var table;
        if(this.props.challenge.userAcceptor != null)
            table = (
                <table className="compForm-table">
                <td className="compForm-td">
                    <p>{this.props.challenge.userCreator.login}</p>
                    <p><a href={this.props.challenge.creatorLink}>Look</a></p>
                    <p>{this.props.challenge.votesForCreator}</p>
                </td>
                <td className="compForm-td">
                    <p>{this.props.challenge.userAcceptor.login}</p>
                    <p><a href={this.props.challenge.acceptorLink}>Look</a></p>
                    <p>{this.props.challenge.votesForAcceptor}</p>
                </td>
            </table>);
        return (
            <div className="compForm">
                <table className="compForm-table">
                    <tr>
                        <p className="compForm-title">{this.props.title}</p>
                        <p>{this.props.challenge.title}</p>
                        <p>{this.props.challenge.description}</p>
                        <p>{this.props.challenge.betSum}</p>
                    </tr>
                    <tr>
                        {table}
                    </tr>
                </table>
            </div>
        );
    }
}

export default CompetitionForm;