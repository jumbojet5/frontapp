import React from 'react';
import ReactDOM from 'react-dom';
import './css/prevCompPage.css';
import Prof from "./profilePage";

class PrevCompPage extends React.Component {
    render() {
        return (
            <div className="prevCompPage">
                <p><button type="submit" onClick={()=>ReactDOM.render(<Prof/>,
                    document.getElementById('root'))}>Back</button></p>
            </div>
        );
    }
}


export default PrevCompPage;