import React from 'react';
import ReactDOM from 'react-dom';
import './css/signInPage.css';
import Prof from "./profilePage";

class SignInPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {token: {}};
    }

    /*eslint-env jquery*/
    handleSubmit(newData) {
        $.ajax({
            url: '/login',
            dataType: 'json',
            type: 'POST',
            data: newData,
            success: function(data) {
                this.setState({token: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('/login', status, err.toString());
            }
        });
    }

    editData(){
        const login=document.getElementById("loginInputS").value;
        const password=document.getElementById("passwordInputS").value;
        if (!login || !password) {
            return false;
        }
        this.handleSubmit({login:login, password:password});
        return true;
    }

    render() {
        return (
            <div className="signInPage">
                <p className="signInPage-title">Sign In</p>
                <p className="signInPage">Login</p>
                <input className="signInPage-input" type="text" size={40} id="loginInputS"/>
                <p className="signInPage">Password</p>
                <input className="signInPage-input" type="password" size={40} id="passwordInputS"/>
                <p><button className="signInPage-button" type="submit" onClick={()=>{
                    if(this.editData())
                        ReactDOM.render(<Prof token={this.state.token}/>, document.getElementById('root'))
                }}>Confirm</button></p>
            </div>
        );
    }
}


export default SignInPage;