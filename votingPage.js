import React from 'react';
import './css/votingPage.css';
import CompetitionForm from "./competitionForm";

class VotingForm extends  React.Component{
    constructor(props) {
        super(props);
        this.state = {displayB:"block",};
        this.token="";
    }

    editData(id){
        const range=document.getElementById("input"+id).value;
        if(range>50)
            this.handleSubmit({challenge: id, voteFor: 2});
        else if(range<50)
            this.handleSubmit({challenge: id, voteFor: 1});
        else return false;
        return true;
    }

    /*eslint-env jquery*/
    handleSubmit(newData) {
        $.ajax({
            url: '/vote',
            dataType: 'json',
            type: 'POST',
            headers:this.token,
            data: newData,
            // success: function(data) {
            //     this.setState({token: data});
            // }.bind(this),
            error: function(xhr, status, err) {
                console.error('/vote', status, err.toString());
            }
        });
    }

    render(){
        this.token=this.props.token;
        return(
            <div className="votingPage">
                <CompetitionForm challenge={this.props.challenge} title="Make Choice"/>
                <div className="votingPage-divVote" id="vote" style={{display:this.state.displayB}}>
                    <input className="votingPage-range" type="range" min="0" max="100" step="50"
                           id={"input"+this.props.challenge.id}/>
                    <p><button className="votingPage-button" type="submit" onClick={()=>{
                        if(this.editData(this.props.challenge.id))
                            this.setState({displayB:"none"})}
                    }>Vote</button></p>
                </div>
            </div>
        );
    }
}
class VotingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: []};
        this.token="";
    }
    /*eslint-env jquery*/
    loadFromServer() {
    $.ajax({
        url: "/challenges",
        dataType: 'json',
        cache: false,
        headers:this.token,
        success: function(data) {
            this.setState({data: data});
        }.bind(this),
        error: function(xhr, status, err) {
            console.error("/challenges", status, err.toString());
        }
    });
    }

    componentDidMount() {
        this.loadFromServer();
    }

    render() {
        this.token = this.props.token;
        const token = this.token;
        const compNodes = this.state.data.map(function(comp) {
            return (
                <VotingForm challenge={comp} token={token}/>
            )
        });
        return (
            <div>
                {compNodes}
            </div>
        )
    }
}

export default VotingPage;