import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import Reg from './registrationPage'
import registerServiceWorker from './registerServiceWorker';
import SignInPage from "./signInPage";

class Main extends React.Component{
    render(){
        return(
            <div>
                <table width="100%">
                    <td width="50%">
                        <Reg/>
                    </td>
                    <td width="50%">
                        <SignInPage/>
                    </td>
                </table>
            </div>
        )
    }
}

ReactDOM.render(<Main/>, document.getElementById('root'));
registerServiceWorker();
