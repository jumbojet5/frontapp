import React from 'react';
import ReactDOM from 'react-dom';
import './css/headerPage.css';
import InvitesPage from "./invitesPage";
import img from './logo.svg';
import ProfilePage from "./profilePage";
import VotingPage from "./votingPage";

class HeaderPage extends React.Component {
    render() {
        return (
            <div><table className="headerPage-table">
                <td className="headerPage-td" width={200}><div className="headerPageLeft">
                    <p><img src={img}  alt = "home" width="32" height="32"/></p>
                    <p><button className="profilePage-button" onClick={()=>ReactDOM.render(
                        <InvitesPage  token={this.props.token}/>, document.getElementById('root'))}>Invites</button></p>
                </div></td>
                <td className="headerPage-td"><div className="headerPageMid">
                    <VotingPage token={this.props.token}/>
                </div></td>
                <td className="headerPage-td"><div className="headerPageRight">
                    <p><button onClick={()=>ReactDOM.render(
                        <ProfilePage token={this.props.token}/>, document.getElementById('root'))}>
                        <img src={img}  alt = "home" width="32" height="32"/></button></p>
                </div></td>
            </table></div>
        );
    }
}

export default HeaderPage;