import React from 'react';
import ReactDOM from 'react-dom';
import './css/profilePage.css';
import img from './logo.svg';
import EditProfilePage from "./editProfilePage";
import ActiveCompPage from "./activeCompPage";
import InvitesPage from "./invitesPage";
import HeaderPage from "./headerPage";
import CreateCompPage from "./createCompPage";

class ProfilePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data : {
                id: "",
                firstName: "",
                lastName: "",
                interests: "",
                login: "",
                avatar: ""
        }};
        this.token="";
    }
    /*eslint-env jquery*/
    loadFromServer() {
         $.ajax({
            url: "/user/profile",
            dataType: 'json',
            cache: false,
            headers:this.token,
            success: function(data) {
                this.setState({data : data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error("/user/profile", status, err.toString());
            }
        });
    }

    componentDidMount() {
        this.loadFromServer();
        setInterval(this.loadFromServer, 2000);
    }

    render() {
        this.token = this.props.token;
        return (
            <div><table className="profilePage-table">
                <td className="headerPage-td" width={200}><div className="profilePageLeft">
                        <p><button className="profilePage-button" onClick={()=>ReactDOM.render(
                            <EditProfilePage token={this.token}/>, document.getElementById('root'))}>Edit</button></p>
                        <p><button className="profilePage-button" onClick={()=>ReactDOM.render(
                            <ActiveCompPage token={this.token}/>, document.getElementById('root'))}>Active</button></p>
                        <p><button className="profilePage-button" onClick={()=>ReactDOM.render(
                            <InvitesPage token={this.token}/>, document.getElementById('root'))}>Invites</button></p>
                        <p><button className="profilePage-button" onClick={()=>ReactDOM.render(
                            <CreateCompPage token={this.token} userCreator={this.state.data}/>,
                            document.getElementById('root'))}>Create Competition</button></p>
                    </div></td>
                    <td className="headerPage-td"><div className="profilePageMid">
                        <p>Info</p>
                        <p>First Name: {this.state.data.firstName}</p>
                        <p>Last Name: {this.state.data.lastName}</p>
                        <p>Login: {this.state.data.login}</p>
                        <p>Interests: {this.state.data.interests}</p>
                    </div></td>
                    <td className="headerPage-td"><div className="profilePageRight">
                        <p><button onClick={()=>ReactDOM.render(<HeaderPage token={this.token}/>,
                            document.getElementById('root'))}>
                            <img src={img}  alt = "home" width="32" height="32"/></button></p>
                    </div></td>
            </table></div>
        );
    }
}


export default ProfilePage;